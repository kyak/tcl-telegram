# Tcl interface to Telegram Bot API

This is a library of helper functions to use Telegram Bot API from your Tcl scripts.
It is designed to be lightweight, fast and easy to use.

Who am I kidding? The mere fact that this abomination works is a miracle! But feel free to improve it by sending feedback and patches.

If you are brave enough to try it out, here's a few examples:

## Calling getUpdates in a synchronous manner and dispatching results

```tcl
set botToken "your telegram bot token here"
source "[file normalize [file dirname $argv0]]/tcl-telegram.tcl"

proc processMessage {message} {
        set from_id [dict getnull $message "from" "id"]
        set from_firstname [dict getnull $message "from" "first_name"]
        set from_lastname [dict getnull $message "from" "last_name"]
        set from_username [dict getnull $message "from" "username"]
        set chat_id [dict getnull $message "chat" "id"]
        set text [dict getnull $message "text"]
        
        if {$text eq "/start"} {
            tg::sendMessage $::botToken $chat_id \
                "Hello! All I can do is nothing."
        } elseif { [regexp {^ping } $text] } {
            tg::sendMessage $::botToken $chat_id \
                "pong: $text"
        } else {
        }
}

proc tgGetUpdates {} {
    set results [tg::getUpdates $::botToken]
    foreach result $results {
        # Dispatch results to various handlers
        set message [dict getnull $result "message"]
        if {$message != ""} {processMessage $message}
        set callback_query [dict getnull $result "callback_query"]
        if {$callback_query != ""} {processCallbackQuery $callback_query}
        set inline_query [dict getnull $result "inline_query"]
        if {$inline_query != ""} {processInlineQuery $inline_query}
    }
}

proc tgloop {} {
    if { [catch {tgGetUpdates}] } {
        puts $::errorInfo
    }
    # Schedule another round of getting updates
    after 1000
    tailcall tgloop
}

tgloop
vwait forever
```

## Doing the same in asynchronous (non-blocking) manner. This allows you to basically have more than one bot running from a single script.

```tcl
set botToken "your telegram bot token here"
source "[file normalize [file dirname $argv0]]/tcl-telegram.tcl"

proc processMessage {message} {
        set from_id [dict getnull $message "from" "id"]
        set from_firstname [dict getnull $message "from" "first_name"]
        set from_lastname [dict getnull $message "from" "last_name"]
        set from_username [dict getnull $message "from" "username"]
        set chat_id [dict getnull $message "chat" "id"]
        set text [dict getnull $message "text"]
        
        if {$text eq "/start"} {
            tg::sendMessage $::botToken $chat_id \
                "Hello! All I can do is nothing."
        } elseif { [regexp {^ping } $text] } {
            tg::sendMessage $::botToken $chat_id \
                "pong: $text"
        } else {
        }
}

proc tgGetUpdates {results} {
    # Schedule another round of updates no matter what happens here
    tg::getUpdates_nb $::botToken tgGetUpdates

    if { [catch {

    # Process results for this round
    foreach result $results {
        # Dispatch results to various handlers
        set message [dict getnull $result "message"]
        if {$message != ""} {processMessage $message}
        set callback_query [dict getnull $result "callback_query"]
        if {$callback_query != ""} {processCallbackQuery $callback_query}
        set inline_query [dict getnull $result "inline_query"]
        if {$inline_query != ""} {processInlineQuery $inline_query}
    }

    }] } {
        puts $::errorInfo
    }
}

# Kick out the first round of updates
tgGetUpdates ""
vwait forever
```
