# Helper functions for Telegram interface

# Used packages
encoding system utf-8
package require tls
package require http
::http::register https 443 [list ::tls::socket -tls1 1]
package require json
package require json::write
package require dicttool

namespace eval tg {

set apiurl "https://api.telegram.org/bot"
array set offset {}
set longpoll "50"
set curlopt {}

# # #
# Internal functions. Don't use these directly.
# # #

# Get message offset for this token
proc getoffset {token} {
	set result [array get tg::offset $token]
	if {$result == ""} {
		set offset 0
	} else {
		set offset [lindex $result 1]
	}
	return $offset
}

# Set message offset for this token
proc setoffset {token offset} {
	set tg::offset($token) $offset
}

# Query data from API
proc query {url query {timeout 15000}} {
	if { [catch {
		set tok [::http::geturl $url -query $query -timeout $timeout -type application/json]
		set data [::http::data $tok]
		::http::cleanup $tok
		set data [tg::ok $data]
	}] } {
		puts $::errorInfo
		return ""
	}
	return $data
}

# Query data from API (non-blocking)
proc query_nb {url query timeout token callback} {
	if { [catch {::http::geturl $url -query $query -timeout $timeout -type application/json \
		-command [list tg::query_nb_handler $token $callback]}] } {
		puts $::errorInfo
		# We didn't call user's callback due to http error, so call it directly
		after 2000
		tailcall $callback ""
	}
	return ""
}

# Handle routine work, then pass on control to user's callback
proc query_nb_handler {token callback http_token} {
	if { [catch {
		set results [tg::ok [::http::data $http_token]]
		# Update message offset
		tg::updOffset $token $results
	}] } {
		puts $::errorInfo
		set results ""
	}
	::http::cleanup $http_token
	# Call to user function
	tailcall $callback $results
}

# Convert JSON data from Telegram to TCL dict
# Returns dictionary of results if success or empty if failed
proc ok {data} {
	if {$data eq ""} {
		puts "Empty data from API!"
		return ""
	}
	set jdata [::json::json2dict $data]
	set ok [dict getnull $jdata "ok"]
	if {$ok eq "true"} {
		#puts "Got data:\n$jdata"
		return [toUnicode [dict getnull $jdata "result"]]
	} else {
		puts "Error getting data from API:\n$jdata"
		return ""
	}
}

# Update message offset
proc updOffset {token results} {
	foreach result $results {
		set update_id [dict getnull $result "update_id"]
		# update message offset
		tg::setoffset $token [expr $update_id+1]
	}
}

# Restore extended unicode sequences from data.
# This is needed because json2dict converts extended unicode sequences
# like \uXXXX\uXXXX to character codes incorrectly (because TCL_UTF_MAX is 3).
proc toUnicode {data} {
	set result ""
	for {set i 0} {$i < [string length $data]} {incr i} {
		set char [string index $data $i]
		set code [scan $char %c]
		if {$code >= 0xD800 && $code <= 0xDBFF} {
			# Recode to \uXXXX
			append result [format "\\u%04.4x" $code]
			# Append low bits
			incr i
			set char [string index $data $i]
			set code [scan $char %c]
			append result [format "\\u%04.4x" $code]
		} else {
			# No change
			append result $char
		}
	}
	return $result
}

# # #
# External functions. Go ahead and use those
# # #

# Convert TCL string to proper JSON string
proc jString {str} {
	set result ""
	# json::write does escaping for 8-bit characters and adds quotes, but doesn't handle unicode
	set str [json::write string [subst -nocommands -novariables $str]]
	# Convert everything non 8-bit to \uXXXX sequences
	for {set i 0} {$i < [string length $str]} {incr i} {
		set char [string index $str $i]
		scan $char %c code
		if {$code > 127} {
			append result [format "\\u%04.4x" $code]
		} else {
			append result $char
		}
	}
	return $result
}

# Get basic information about the bot
# Returns User object (https://core.telegram.org/bots/api#user)
proc getMe {token} {
	set url "$tg::apiurl$token/getMe"
	set result [tg::query $url ""]
	return $result
}

# Send messages
# Returns sent Message object (https://core.telegram.org/bots/api#message)
proc sendMessage {token chat_id text {parse_mode ""} {reply_markup ""}} {
	set url "$tg::apiurl$token/sendMessage"
	set query [json::write object chat_id $chat_id text [jString $text] parse_mode [jString $parse_mode] reply_markup [jString $reply_markup]]
	set result [tg::query $url $query]
	return $result
}

# Get updates
# Returns the list of incoming Update objects (https://core.telegram.org/bots/api#update)
# Optional argument `allowed_updates` must be TCL list of updates types, see https://core.telegram.org/bots/api#getupdates
proc getUpdates {token {allowed_updates "\[\]"}} {
	set url "$tg::apiurl$token/getUpdates"
	set offset [tg::getoffset $token]
	if {$allowed_updates != "\[\]"} {
		set jstr {}
		foreach up $allowed_updates {lappend jstr [jString $up]}
		set allowed_updates [json::write array [join $jstr ","]]
	}
	set query [json::write object offset $offset timeout $tg::longpoll allowed_updates $allowed_updates]
	# Set http timeout twice as large as long polling period for Telegram
	set results [tg::query $url $query [expr $tg::longpoll*1000*2]]
	# Update message offset
	updOffset $token $results
	return $results
}

# Get updates (non-blocking)
# Schedules user callback to be called when new updates are ready. User's callback functions will be called with the list of incoming Update objects (https://core.telegram.org/bots/api#update)
# Optional argument `allowed_updates` must be TCL list of updates types, see https://core.telegram.org/bots/api#getupdates
proc getUpdates_nb {token callback {allowed_updates "\[\]"}} {
	set url "$tg::apiurl$token/getUpdates"
	set offset [tg::getoffset $token]
	if {$allowed_updates != "\[\]"} {
		set jstr {}
		foreach up $allowed_updates {lappend jstr [jString $up]}
		set allowed_updates [json::write array [join $jstr ","]]
	}
	set query [json::write object offset $offset timeout $tg::longpoll allowed_updates $allowed_updates]
	# Set http timeout twice as large as long polling period for Telegram
	tg::query_nb $url $query [expr $tg::longpoll*1000*2] $token $callback
}

# Answer inline query
proc answerInlineQuery {token inline_query_id inlineQueryResults} {
	set url "$tg::apiurl$token/answerInlineQuery"
	set query [json::write object inline_query_id $inline_query_id results $inlineQueryResults]
	set result [tg::query $url $query]
	return $result
}

# Send audio
# Returns sent Message object (https://core.telegram.org/bots/api#message)
proc sendAudio {token chat_id fname duration performer title thumb} {
	set url "$tg::apiurl$token/sendAudio"
	set cmd [list curl {*}$tg::curlopt -s -S -m 15 --fail -F chat_id=$chat_id -F audio=@\"$fname\" -F duration=$duration -F performer=\"$performer\" -F title=\"$title\" -F thumb=@\"$thumb\" $url]
	if { [catch {set results [exec {*}$cmd]}] } {
		puts $::errorInfo
		return ""
	} else {
		return [::json::json2dict $results]
	}
}

# Delete message
proc deleteMessage {token chat_id message_id} {
	set url "$tg::apiurl$token/deleteMessage"
	set query [json::write object chat_id $chat_id message_id $message_id]
	set result [tg::query $url $query]
	return $result
}

# Forward message
# Returns sent Message object (https://core.telegram.org/bots/api#message)
proc forwardMessage {token chat_id from_chat_id message_id} {
	set url "$tg::apiurl$token/forwardMessage"
	set query [json::write object chat_id $chat_id from_chat_id $from_chat_id message_id $message_id]
	set result [tg::query $url $query]
	return $result
}

# Answer callback query
proc answerCallbackQuery {token callback_query_id text} {
	set url "$tg::apiurl$token/answerCallbackQuery"
	set query [json::write object callback_query_id [jString $callback_query_id] text [jString $text]]
	set result [tg::query $url $query]
	return $result
}

# Edit reply_markup of inline message
# Returns sent Message object (https://core.telegram.org/bots/api#message)
proc editMessageReplyMarkup {token chat_id message_id inline_message_id reply_markup} {
	set url "$tg::apiurl$token/editMessageReplyMarkup"
	if {$inline_message_id != ""} {
		set query [json::write object inline_message_id [jString $inline_message_id] reply_markup $reply_markup]
	} else {
		set query [json::write object chat_id $chat_id message_id $message_id reply_markup $reply_markup]
	}
	set result [tg::query $url $query]
	return $result
}

# Edit text of inline message
# Returns sent Message object (https://core.telegram.org/bots/api#message)
proc editMessageText {token chat_id message_id inline_message_id text parse_mode reply_markup} {
	set url "$tg::apiurl$token/editMessageText"
	if {$inline_message_id != ""} {
		set query [json::write object inline_message_id [jString $inline_message_id] reply_markup $reply_markup text [jString $text] parse_mode [jString $parse_mode]]
	} else {
		set query [json::write object chat_id $chat_id message_id $message_id reply_markup $reply_markup text [jString $text] parse_mode [jString $parse_mode]]
	}
	set result [tg::query $url $query]
	return $result
}

# Ban chat member
proc banChatMember {token chat_id user_id} {
	set url "$tg::apiurl$token/banChatMember"
	set query [json::write object chat_id $chat_id user_id $user_id]
	set result [tg::query $url $query]
	return $result
}

# Restrict chat member
proc restrictChatMember {token chat_id user_id} {
	set url "$tg::apiurl$token/restrictChatMember"
	set query [json::write object chat_id $chat_id user_id $user_id \
		can_send_messages false can_send_media_messages false \
		can_send_other_messages false can_add_web_page_previews false]
	set result [tg::query $url $query]
	return $result
}

# Unrestrict chat member
proc unRestrictChatMember {token chat_id user_id} {
	set url "$tg::apiurl$token/restrictChatMember"
	set query [json::write object chat_id $chat_id user_id $user_id \
		can_send_messages true can_send_media_messages true \
		can_send_other_messages true can_add_web_page_previews true]
	set result [tg::query $url $query]
	return $result
}

# Get number of chat members
# Returns Int on success
proc getChatMembersCount {token chat_id} {
	set url "$tg::apiurl$token/getChatMembersCount"
	set query [json::write object chat_id $chat_id]
	set result [tg::query $url $query]
	return $result
}

# Get chat information
# Returns Chat object (https://core.telegram.org/bots/api#chat)
proc getChat {token chat_id} {
	set url "$tg::apiurl$token/getChat"
	set query [json::write object chat_id $chat_id]
	set result [tg::query $url $query]
	return $result
}

proc getChatS {token chat_id} {
	set url "$tg::apiurl$token/getChat"
	set query [json::write object chat_id [jString $chat_id]]
	set result [tg::query $url $query]
	return $result
}
# Get information about chat member
# Returns ChatMember object (https://core.telegram.org/bots/api#chatmember)
proc getChatMember {token chat_id user_id} {
	set url "$tg::apiurl$token/getChatMember"
	set query [json::write object chat_id $chat_id user_id $user_id]
	set result [tg::query $url $query]
	return $result
}

}
